<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{secure_asset('img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{auth()->user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="{{activeMenu('home')}}">
                <a href="{{(env('SSL',false))?secure_url(route('home')):route('home')}}"> <i
                        class="fa fa-home"></i><span>Dashboard</span> </a>
            </li>
            <li class="">

                <a href="{{route('pos.pos')}}"> <i class="fa fa-shopping-cart"></i><span>POS</span> </a>

            </li>
            <li class="treeview {{areActiveRoutes(['category.category','subcategory.subcategory','product.make','product.products'])}}">
                <a href="#">
                    <i class="fa fa-th-list"></i> <span>Product</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{activeMenu('product.make')}}"><a href="{{make_ssl_route('product.make')}}"><i
                                class="fa fa-align-justify"></i> Add Product
                        </a></li>
                    <li class="{{activeMenu('product.products')}}"><a href="{{make_ssl_route('product.products')}}"><i
                                class="fa  fa-list-alt"></i> Product List </a></li>
                    <li class="{{activeMenu('category.category')}}">
                        <a href="{{(env('SSL',false))?secure_url(route('category.category')):route('category.category')}}">
                            <i class="fa  fa-indent"></i>Category List
                        </a>
                    </li>
                    <li class="{{activeMenu('subcategory.subcategory')}}"><a
                            href="{{(env('SSL',false))?secure_url(route('subcategory.subcategory')):route('subcategory.subcategory')}}"><i
                                class="fa fa-outdent"></i>Sub Category List </a></li>
                    <li><a href="damageproduct.php"><i class="fa fa-columns"></i> Damage Product </a></li>
                    <li><a href="producttransfer.php"><i class="fa fa-repeat"></i> Product Transfer</a></li>
                    <li><a href=".php"><i class="fa fa-print"></i> Print Barcode </a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-heart"></i> <span>Sales</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="newsales.php"><i class="fa fa-plus-square"></i> New Sales </a></li>
                    <li><a href="saleslist.php"><i class="fa fa-reorder"></i> Sales List </a></li>
                </ul>
            </li>
            <li class="treeview {{activeMenu('vendor.vendor')}}">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Vendor</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{activeMenu('vendor.vendor')}}"><a href="{{make_ssl_route('vendor.vendor')}}"><i
                                class="fa fa-tasks"></i> Vendor List </a></li>
                </ul>
            </li>
            <li class="treeview {{areActiveRoutes(['brand.brands'])}}">
                <a href="#">
                    <i class="fa fa-codepen"></i> <span>Brand</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{activeMenu('brand.brands')}}"><a href="{{make_ssl_route('brand.brands')}}"><i
                                class="fa fa-list-ol"></i> Brand List </a></li>
                </ul>
            </li>

            <li class="treeview {{areActiveRoutes(['customer.customer'])}}">
                <a href="#">
                    <i class="fa  fa-user"></i><span>Customer</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{activeMenu('customer.customer')}}"><a href="{{route('customer.customer')}}"><i
                                class="fa fa-tasks"></i>Customer List</a></li>
                </ul>
            </li>
            <li class="treeview {{areActiveRoutes(['expenses.category','expenses.expenses'])}}">
                <a href="#">
                    <i class="fa fa-user"></i><span>Expenses</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{activeMenu('expenses.expenses')}}"><a href="{{make_ssl_route('expenses.expenses')}}"><i
                                class="fa fa-tasks"></i>Expenses List</a></li>
                    <li class="{{activeMenu('expenses.category')}}"><a href="{{make_ssl_route('expenses.category')}}"><i
                                class="fa fa-tasks"></i>Expenses Category</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa  fa-user"></i><span>SMS</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="sendsms.php"><i class="fa fa-tasks"></i>Send SMS</a></li>
                    <li><a href="smsreport.php"><i class="fa fa-tasks"></i>SMS Report</a></li>
                    <li><a href="smssetting.php"><i class="fa fa-tasks"></i>SMS Setting</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa  fa-user"></i><span>eMail</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="sendeemail.php"><i class="fa fa-tasks"></i>Send eMail</a></li>
                    <li><a href="emailreport.php"><i class="fa fa-tasks"></i>eMail Report</a></li>
                    <li><a href="emailsetting.php"><i class="fa fa-tasks"></i>eMail Setting</a></li>
                </ul>
            </li>

            <li class="treeview {{areActiveRoutes(['hrm.department.department','hrm.employee.employee'])}}">
                <a href="#">
                    <i class="fa fa-user"></i><span>HRM</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{activeMenu('hrm.department.department')}}">
                        <a href="{{make_ssl_route('hrm.department.department')}}"><i class="fa fa-tasks"></i>Department</a>
                    </li>
                    <li class="{{activeMenu('hrm.employee.employee')}}">
                        <a href="{{route('hrm.employee.employee')}}">
                            <i class="fa fa-tasks"></i>Employee
                        </a>
                    </li>
                    <li><a href="payrolllist.php"><i class="fa fa-tasks"></i>All Payment</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i><span>Accounts</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="bankaccount.php"><i class="fa fa-tasks"></i>Bank Account</a></li>
                    <li><a href="bankdeposit.php"><i class="fa fa-tasks"></i>Bank Deposit</a></li>
                    <li><a href="withdrawfrombank.php"><i class="fa fa-tasks"></i>Withdraw From Bank</a></li>
                    <li><a href="balancetransfer.php"><i class="fa fa-tasks"></i>Balance Transfer</a></li>
                    <li><a href="alltransetion.php"><i class="fa fa-tasks"></i>All Transection</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-asterisk"></i> <span>Report</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-bars"></i> Overview Chart </a></li>
                    <li><a href=""><i class="fa fa-bars"></i> Warehouse Stock Chart </a></li>
                    <li><a href=""><i class="fa fa-bars"></i> Sealer Report </a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Product Quantity Alert </a></li>
                    <li><a href=""><i class="fa fa-bars"></i> Total Product Report </a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Daily Sale </a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Monthly Sale </a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Overall Sale Report </a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Payment Report </a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Profit and Loss Statement </a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Purchase Report </a></li>
                    <li><a href=""><i class="fa fa-bars"></i> Daily Purchase </a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Monthly Purchase</a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Expanse Report</a></li>
                    <li><a href=""><i class="fa fa-bars"></i>Vendor Report</a></li>
                </ul>
            </li>
            <li class="treeview  {{areActiveRoutes(['warehouse.warehouse'])}}">
                <a href="#">
                    <span class="glyphicon glyphicon-cog"></span><span>Settings</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="systemsettings.php"><i class="fa fa-bars"></i> System Settings </a></li>
                    <li><a href="changelogo.php"><i class="fa fa-bars"></i> Change Logo </a></li>
                    <li><a href="currencies.php"><i class="fa fa-bars"></i> Currencies </a></li>
                    <li><a href="taxrate.php"><i class="fa fa-bars"></i> Tax Rates</a></li>
                    <li><a href="vatsettings.php"><i class="fa fa-bars"></i> Vat Settings </a></li>
                    <li><a href="setdiscount.php"><i class="fa fa-bars"></i> Set Discount </a></li>
                    <li><a href="expensecategory.php"><i class="fa fa-bars"></i> Expense Category </a></li>
                    <li class="{{activeMenu('warehouse.warehouse')}}">
                        <a href="{{env('SSL')?secure_url(route('warehouse.warehouse')):route('warehouse.warehouse')}}">
                            <i class="fa fa-bars"></i> Warehouses </a></li>
                    <li><a href="warehousetransfer.php"><i class="fa fa-random"></i> Warehouse Transfer </a></li>

                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
