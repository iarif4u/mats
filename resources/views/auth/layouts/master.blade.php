<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{secure_asset('favicon.ico')}}">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">--}}
    <link rel="stylesheet" href="{{(env('SSL',false))?secure_asset('css/all.css'):asset('css/all.css')}}">
    <link rel="stylesheet" href="{{(env('SSL',false))?secure_asset('css/app.css'):asset('css/app.css')}}">
    @yield('style')
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
@include('admin.layouts.include.header')

@include('admin.layouts.include.leftSide')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    @yield('content-header')
    <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <span class="each-error">{{ $error }} </span><br/>
                    @endforeach
                </div>
        @endif
        @if(session()->has('message'))
            {!! html()->div()->class('alert alert-success')->html(session()->get('message')) !!}
        @endif
        <!-- Dynamic Content -->
        @yield('content')
        <!-- /.Dynamic Content -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <!-- To the right -->
{{-- <div class="pull-right hidden-xs">
     Officer CRM DC Project
 </div>--}}
<!-- Default to the left -->
    {!! html()->element('footer')
    ->class('main-footer')
    ->child( html()->element('strong')->text('Copyright &copy; '.date('Y '))
    ->child(html()->a('https://alorpathshala.org',env('APP_NAME','Laravel')))
    ->child(html()->span(' All rights reserved.')))
    !!}


</div>
@yield('modal')
<!-- Javascript Start -->
{!! html()->element('script')->attribute('src',(env('SSL',false))?secure_asset('js/all.js'):asset('js/all.js')) !!}
{!! html()->element('script')->attribute('src',(env('SSL',false))?secure_asset('js/app.js'):asset('js/app.js')) !!}
@yield('script')
<!-- /.Javascript End -->
</body>
</html>
