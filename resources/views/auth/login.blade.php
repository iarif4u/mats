<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>


    <link rel="shortcut icon" href="images/favicon.png">
    <title>Mats</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/fontawesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="vendor/animateit/animate.min.css" rel="stylesheet">

    <!-- Vendor css -->
    <link href="vendor/owlcarousel/owl.carousel.css" rel="stylesheet">
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Template base -->
    <link href="css/theme-base.css" rel="stylesheet">

    <!-- Template elements -->
    <link href="css/theme-elements.css" rel="stylesheet">


    <!-- Responsive classes -->
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- Template color -->
    <link href="css/color-variations/blue.css" rel="stylesheet" type="text/css" media="screen" title="blue">

    <!-- LOAD GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800"
          rel="stylesheet" type="text/css"/>

    <!-- CSS CUSTOM STYLE -->
    <link rel="stylesheet" type="text/css" href="css/custom.css" media="screen"/>

    <!--VENDOR SCRIPT-->
    <script src="vendor/jquery/jquery-1.11.2.min.js"></script>
    <script src="vendor/plugins-compressed.js"></script>

</head>

<body class="wide">


<!-- WRAPPER -->
<div class="wrapper">

    <!-- SECTION -->
    <section class="parallax fullscreen" style="background-image: url({{asset('images/slider/homee.jpg')}})">
        <div class="container container-fullscreen">
            <div class="text-middle">
                <div class="row">
                    <div class="col-md-4 center p-30 background-white">
                        <h3>Login to your Account</h3>

                        <form method="POST" action="{{ route('login') }}" class="form-transparent-grey">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label class="sr-only">Username or Email</label>
                                <input placeholder="Email Address" id="email" type="email"
                                       class="form-control @error('email') is-invalid @enderror" name="email"
                                       value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group m-b-5">
                                <label class="sr-only">Password</label>
                                <input placeholder="Password" id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <div class="form-group form-inline text-left ">

                                <div class="checkbox">
                                    <label>
                                        <input class="form-check-input" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <small> Remember me</small>
                                    </label>
                                </div>
                                @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}" class="right">
                                    <small>Lost your Password?</small>
                                </a>
                                @endif
                            </div>
                            <div class="text-left form-group">
                                <input type="submit" value="Login" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END: SECTION -->


</div>
<!-- END: WRAPPER -->

<!-- GO TOP BUTTON -->
<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>

<!-- Theme Base, Components and Settings -->
<script src="js/theme-functions.js"></script>

<!-- Custom js file -->
<script src="js/custom.js"></script>


</body>

</html>
a
