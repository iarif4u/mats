<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>


    <link rel="shortcut icon" href="images/favicon.png">
    <title>Mats</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/fontawesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="vendor/animateit/animate.min.css" rel="stylesheet">

    <!-- Vendor css -->
    <link href="vendor/owlcarousel/owl.carousel.css" rel="stylesheet">
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Template base -->
    <link href="css/theme-base.css" rel="stylesheet">

    <!-- Template elements -->
    <link href="css/theme-elements.css" rel="stylesheet">

    <!-- Responsive classes -->
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->


    <!-- Template color -->
    <link href="css/color-variations/green.css" rel="stylesheet" type="text/css" media="screen">

    <!-- LOAD GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800"
          rel="stylesheet" type="text/css"/>


    <!-- CSS real-estate STYLE -->
    <link rel="stylesheet" type="text/css" href="homepages/real-estate/css/real-estate-style.css" media="screen"/>

    <!-- CSS CUSTOM STYLE -->
    <link rel="stylesheet" type="text/css" href="css/custom.css" media="screen"/>

    <!--VENDOR SCRIPT-->
    <script src="vendor/jquery/jquery-1.11.2.min.js"></script>
    <script src="vendor/plugins-compressed.js"></script>
    <style>
        #header-wrap {
            background: #15a74c !important;
        }

        #mainMenu > ul > li > a {
            color: white !important;
        }

        #gallery {
            padding-top: 0 !important;
        }

        .background-colored {
            background-color: #15A74C !important;
        }

        ul.dropdown-menu {
            background: #15A74C !important;
        }

        ul.dropdown-menu > li > a {
            color: white !important;
        }
    </style>
</head>

<body class="wide">


<!-- WRAPPER -->
<div class="wrapper">


    <!-- HEADER -->
    <header id="header" class="header-modern">
        <div id="header-wrap">
            <div class="">

                <!--LOGO-->
                <div id="logo">
                    <a href="index.php" class="logo" data-dark-logo="images/mats_logo.png">
                        <img src="images/mats_logo.png">
                    </a>
                </div>
                <!--END: LOGO-->

                <!--MOBILE MENU -->
                <div class="nav-main-menu-responsive">
                    <button class="lines-button x">
                        <span class="lines"></span>
                    </button>
                </div>
                <!--END: MOBILE MENU -->

                <!--NAVIGATION-->
                <div class="navbar-collapse collapse main-menu-collapse navigation-wrap">
                    <div class="container">
                        <nav class="main-menu mega-menu" id="mainMenu">
                            <ul class="main-menu nav nav-pills">
                                <li class="dropdown">
                                    <a href="#header" class="scroll-to">Home</a>
                                </li>
                                <li class="dropdown"><a href="#program" class="scroll-to">About The MATS</a></li>
                                <li class=""><a href="{{route('governing_body')}}" class="">Governing Body</a></li>
                                <li class="dropdown"><a href="#mission" class="scroll-to">Mission & Vision</a></li>
                                <li class="dropdown"><a href="#">Facilities <i class="fa fa-angle-down"></i> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Academic</a></li>
                                        <li><a href="#">Administrative</a></li>
                                        <li><a href="#">Lab</a></li>
                                        <li><a href="#">Hospital</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown"><a href="#teacher" class="scroll-to">Teachers List</a></li>
                                <li class="dropdown"><a href="#section6" class="scroll-to">Students List</a></li>
                                <li class="dropdown"><a href="#section6" class="scroll-to">Activates</a></li>
                                <li class="dropdown"><a href="#gallery" class="scroll-to">Gallery</a></li>
                                <li class="dropdown"><a href="#section6" class="scroll-to">Contact US</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--END: NAVIGATION-->


            </div>
        </div>
    </header>
    <!-- END: HEADER -->


    <!-- SLIDER -->
    <section id="slider" class="no-padding">

        <div id="slider-carousel" class="boxed-slider">

            <div style="background-image:url('images/slider/homee.jpg');" class="owl-bg-img">

            </div>
            <div style="background-image:url('images/slider/home4.jpg');" class="owl-bg-img">


            </div>
            <div style="background-image:url('images/slider/home1.jpg');" class="owl-bg-img">


            </div>
        </div>
    </section>
    <!-- END: SLIDER -->


    <!-- FORM -->
    <div id="program" class="background-grey">
        <div class="container p-t-60 p-b-60">
            <div class="row">
                <div class="col-md-12">

                    <h2 class="text-medium">WELCOME TO THE WORLD OF Mats</h2>
                    <p>২০০৭ সালে ঢাকার প্রাণকেন্দ্র শ্যামলীতে ট্রমা সেন্টার মেডিকেল ইনিস্টিটিউটস এর অগ্রযাত্রা ।
                        স্বাস্থ্য ও পরিবার কল্যাণ মন্ত্রণালয় কর্তৃক অনুমোদন লাভ করে বাংলাদেশ রাষ্ট্রীয় চিকিৎসা অনুষদের
                        অধীনে ২০০৭-২০০৮ শিক্ষাবর্ষ হতে মাত্র ১৫ জন ছাত্র/ছাত্রী নিয়ে প্রতিষ্ঠানটির আত্মপ্রকাশ। এই
                        স্বপ্নযাত্রার দূর্গম পথ পাড়ি দেওয়ার সহযোগী হিসাবে পান একদল মেধাবী, দক্ষ ও অসম্ভব পরিশ্রমী কর্ম
                        কর্মকর্তা কর্মচারী এবং শিক্ষক মন্ডলী। কলেজ টিতে লেখাপড়ার সুষ্ঠু পরিবেশ, শিক্ষার্থীদের একাডেমিক
                        শিক্ষার পাশাপাশি শিক্ষার্থীদের শারীরিক ও মানসিক প্রতিভা বিকাশ ও নৈতিক শিক্ষা লাভের সব ধরনের
                        ব্যবস্থা থাকবে এ ধরনের প্রত্যয় নিয়েই কলেজটির যাত্রা শুর হয়েছে। সব সময় রাজনীতি, ধুমপান ও সন্ত্রাস
                        মুক্ত পরিবেশ বিরাজমান। এ দীর্ঘ যাত্রা পথে সংযোজিত হয়েছে আরো ৭টি প্রতিষ্ঠান। সর্বোপরি ট্রমা
                        মেডিকেল আজ এক বিশাল পরিবার । দেশে বর্তমানে দক্ষ ও আদর্শবান মেডিকেল এ্যাসিসট্যান্ট ও টেকনোলজিষ্ট
                        এর প্রয়োজন প্রকট। সঠিক স্বাস্থ্যসেবা নিশ্চিত কল্পে উল্লেখিত দক্ষ জনবল তৈরীর দৃঢ় প্রত্যয়ে প্রতি
                        বছর আমাদের পাঁচটি শাখা থেকে বের হচ্ছে দক্ষ মেডিকেল এ্যাসিসট্যান্ট ও টেকনোলজিষ্ট।</p>
                </div>
            </div>
        </div>
    </div>
    <!-- END: FORM -->

    <section id="mission">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Mission</h2>
                    <div class="portfolio-client-description">
                        <p> To promote health and well-being through excellence in diagnosisof diseases</p>
                        <p> To create professionals for providing health care assistanceto the peopleof rural and hard to reach area</p>

                        <p>To make effective contributionsto healthcaresystem of Bangladesh</p>
                        <p>To supply the nation with expert diploma Health technologist, Pharmacist and Diploma Dentist</p>

                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <h2>Vision</h2>
                    <div class="portfolio-client-description">
                        <p>
                            To contribute to the national development by leading the community where everyone can enjoy the right of education, medicalcareand security.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--Team Carousel -->

    <!--Team members (Carousel with description) -->
    <div class="background-grey" id="teacher">
        <div class="container">
            <div class="hr-title hr-long center"><abbr>Teachers List</abbr>
            </div>
            <div class="row carousel-description-style">
                <div class="col-md-4">
                    <div class="description">
                        <h2>Our Teachers</h2> Aliquam enim enim, pharetra in sodales at, interdum sit amet dui. Nullam
                        vulputate
                        euis od urna non pharetra. Phasellus bland matt is ipsum, ac laoreet lorem lacinia et. interum
                        sit
                        amet dui.
                        <br/>
                        <br/> Nullam vulputate euismod urna non pharetra. Nullam vulputate euismod urna non pharetra.
                        Phasellus blandit mattis ipsum, ac laoreet lorem lacinia et.et.
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="carousel" data-lightbox-type="gallery" data-carousel-col="3">
                        <div class="featured-box">
                            <div class="image-box circle-image small">
                                <img alt="" src="{{asset('teachers/Md. Minar Hossain Khan.jpg')}}" class="">
                            </div>
                            <div class="image-box-description text-center">
                                <h4>Md. Minar Hossain Khan</h4>
                                <p class="subtitle">Teacher</p>
                                <hr class="line">
                                <div class="social-icons social-icons-border m-t-10 text-center">
                                    <ul>
                                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="featured-box">
                            <div class="image-box circle-image small">
                                <img src="{{asset('teachers/Prof. Dr. G U Ahsan, PhD.jpg')}}" alt="">
                            </div>
                            <div class="image-box-description text-center">
                                <h4>Prof. Dr. G U Ahsan, PhD</h4>
                                <p class="subtitle">Professor</p>
                                <hr class="line">
                                <div class="social-icons social-icons-border m-t-10 text-center">
                                    <ul>
                                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="featured-box">
                            <div class="image-box circle-image small">
                                <img alt="" src="{{asset('teachers/Prof. Dr. Jonaid Shafiq, PhD.jpg')}}">
                            </div>
                            <div class="image-box-description text-center ">
                                <h4>Prof. Dr. Jonaid Shafiq, PhD</h4>
                                <p class="subtitle">Professor</p>
                                <hr class="line">
                                <div class="social-icons social-icons-border m-t-10 text-center">
                                    <ul>
                                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="featured-box">
                            <div class="image-box circle-image small">
                                <img alt="" src="{{asset('teachers/Prof. Dr. M. A. Wadud Sarker.jpg')}}">
                            </div>
                            <div class="image-box-description text-center">
                                <h4>Prof. Dr. M. A. Wadud Sarker</h4>
                                <p class="subtitle">Professor</p>
                                <hr class="line">
                                <div class="social-icons social-icons-border m-t-10 text-center">
                                    <ul>
                                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="featured-box">
                            <div class="image-box circle-image small">
                                <img alt="" src="{{asset('teachers/Prof. Dr. Sarder A. Nayeem, PhD.jpg')}}">
                            </div>
                            <div class="image-box-description text-center">
                                <h4>Prof. Dr. Sarder A. Nayeem, PhD</h4>
                                <p class="subtitle">Professor</p>
                                <hr class="line">
                                <div class="social-icons social-icons-border m-t-10 text-center">
                                    <ul>
                                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="featured-box">
                            <div class="image-box circle-image small">
                                <img alt="" src="{{asset('teachers/Tanveer Sinha.jpg')}}">
                            </div>
                            <div class="image-box-description text-center">
                                <h4>Tanveer Sinha</h4>
                                <p class="subtitle">Teacher</p>
                                <hr class="line">
                                <div class="social-icons social-icons-border m-t-10 text-center">
                                    <ul>
                                        <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="social-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <hr class="space">
        </div>
    </div>
    <!--Team Carousel -->


    <!-- PORTFOLIO -->
    <section id="gallery" class="p-b-0">
        <div class="portfolio">
            <!-- Portfolio Items -->
            <div id="isotope" class="isotope portfolio-items" data-isotope-item-space="0" data-isotope-mode="masonry"
                 data-isotope-col="4" data-isotope-item=".portfolio-item">
                <div class="portfolio-item design artwork">
                    <div class="image-box effect victor"><img src="images/slider/homee.jpg" alt="">
                        <div class="image-box-content">
                            <h3>Fast Skateboard</h3>
                            <p>Consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
                <div class="portfolio-item design beauty">
                    <div class="image-box effect victor"><img src="images/slider/home4.jpg" alt="">
                        <div class="image-box-content">
                            <h3>Working hard</h3>
                            <p>Consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
                <div class="portfolio-item design beauty">
                    <div class="image-box effect victor"><img src="images/slider/home1.jpg" alt="">
                        <div class="image-box-content">
                            <h3>The feather man</h3>
                            <p>Consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
                <div class="portfolio-item design artwork">
                    <div class="image-box effect victor"><img src="images/slider/home4.jpg" alt="">
                        <div class="image-box-content">
                            <h3>The long line</h3>
                            <p>Consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
                <div class="portfolio-item banner beauty">
                    <div class="image-box effect victor"><img src="images/slider/homee.jpg" alt="">
                        <div class="image-box-content">
                            <h3>Backwards</h3>
                            <p>Consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
                <div class="portfolio-item design artwork banner">
                    <div class="image-box effect victor"><img src="images/slider/home1.jpg" alt="">
                        <div class="image-box-content">
                            <h3>Disappointed horse</h3>
                            <p>Consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
                <div class="portfolio-item design artwork">
                    <div class="image-box effect victor"><img src="images/slider/home4.jpg" alt="">
                        <div class="image-box-content">
                            <h3>Wire's</h3>
                            <p>Consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
                <div class="portfolio-item design marketing">
                    <div class="image-box effect victor"><img src="images/slider/homee.jpg" alt="">
                        <div class="image-box-content">
                            <h3>Forcing</h3>
                            <p>Consectetur adipisicing elit</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Portfolio Items -->

        </div>
    </section>
    <!-- END: PORTFOLIO -->

    <!-- PARTNERS -->
    {{--<section id="section2">
        <div class="container">
            <div class="heading heading-center">
                <h2>Sister Organization</h2>
            </div>

            <ul class="grid grid-5-columns">
                <li>
                    <a href="#"><img src="images/clients/1.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/2.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/3.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/4.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/5.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/6.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/7.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/8.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/9.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/10.png" alt="">
                    </a>
                </li>
            </ul>

        </div>
    </section>--}}
    <!-- PARTNERS -->

    <!-- ABOUT AGENT -->
   {{-- <section id="section3">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <img alt="real-estate" src="homepages/real-estate/images/agent.jpg" class="img-responsive m-b-10">
                </div>
                <div class="col-md-7">
                    <h2 class="text-medium">Juna Smith</h2>
                    <p>২০০৭ সালে ঢাকার প্রাণকেন্দ্র শ্যামলীতে ট্রমা সেন্টার মেডিকেল ইনিস্টিটিউটস এর অগ্রযাত্রা ।
                        স্বাস্থ্য ও পরিবার কল্যাণ মন্ত্রণালয় কর্তৃক অনুমোদন লাভ করে বাংলাদেশ রাষ্ট্রীয় চিকিৎসা অনুষদের
                        অধীনে ২০০৭-২০০৮ শিক্ষাবর্ষ হতে মাত্র ১৫ জন ছাত্র/ছাত্রী নিয়ে প্রতিষ্ঠানটির আত্মপ্রকাশ।</p>
                    <div class="clear"></div>
                    <a class="read-more" href="#">Contact Juna now</a>

                </div>
            </div>

        </div>
    </section>--}}
    <!-- END: ABOUT AGENT -->


    <!-- PROPERTY FEATURES -->
    <section id="section5" class="p-t-100">
        <div class="container">
            <div class="heading heading-center">
                <h2>Authorized Institute</h2>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="icon-box effect medium square color">
                        <div class="icon"><a href="#"><i class="fa fa-user"></i></a></div>
                        <h3>Ministry of Health and Family Welfare</h3>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box effect medium square color">
                        <div class="icon"><a href="#"><i class="fa fa-user"></i></a></div>
                        <h3>BMDC</h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box effect medium square color">
                        <div class="icon"><a href="#"><i class="fa fa-user"></i></a></div>
                        <h3>University of Dhaka</h3>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box effect medium square color">
                        <div class="icon"><a href="#"><i class="fa fa-user"></i></a></div>
                        <h3>The State Medical Faculty of Bangladesh</h3>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box effect medium square color">
                        <div class="icon"><a href="#"><i class="fa fa-user"></i></a></div>
                        <h3>Pharmacy Council of Bangladesh</h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box effect medium square color">
                        <div class="icon"><a href="#"><i class="fa fa-user"></i></a></div>
                        <h3>Bangladesh Open University</h3>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END: PROPERTY FEATURES -->

    <!-- GET FREE CONSULTATION -->
    <section id="section6" class="background-grey">
        <div class="container">
            <div class="row">

                <div class="col-md-9 center background-colored text-light p-40">
                    <div class="text-medium">Contact Us</div>
                    <p>Praesent sagittis risus nisi, a fermentum tellus aliquam vel. Ut faucibus, mauris quis fermentum
                        placerat, sem turpis volutpat massa.</p>

                    <form id="widget-contact-form" action="include/contact-form.php" role="form" method="post">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="name">Name</label>
                                <input type="text" aria-required="true" name="widget-contact-form-name"
                                       class="form-control required name" placeholder="Enter your Name">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">Email</label>
                                <input type="email" aria-required="true" name="widget-contact-form-email"
                                       class="form-control required email" placeholder="Enter your Email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="subject">Phone</label>
                                <input type="text" name="widget-contact-form-phone" class="form-control"
                                       placeholder="Enter your phone number">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="subject">Company</label>
                                <input type="text" name="widget-contact-form-company" class="form-control"
                                       placeholder="Enter your Company name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea type="text" name="widget-contact-form-message" rows="9"
                                      class="form-control required" placeholder="Enter your Message"></textarea>
                        </div>
                        <div class="row">
                            <div class="form-group text-center">
                                <input type="text" class="hidden" id="widget-contact-form-antispam"
                                       name="widget-contact-form-antispam" value=""/>
                                <button class="btn button-light center" type="submit" id="form-submit">Send message
                                </button>
                            </div>
                        </div>
                    </form>
                    <script type="text/javascript">
                        jQuery("#widget-contact-form").validate({

                            submitHandler: function (form) {

                                jQuery(form).ajaxSubmit({
                                    success: function (text) {
                                        if (text.response == 'success') {
                                            $.notify({
                                                message: "We have <strong>successfully</strong> received your Message and will get Back to you as soon as possible."
                                            }, {
                                                type: 'success'
                                            });
                                            $(form)[0].reset();

                                        } else {
                                            $.notify({
                                                message: text.message
                                            }, {
                                                type: 'danger'
                                            });
                                        }
                                    }
                                });
                            }
                        });

                    </script>
                </div>

            </div>
        </div>
    </section>
    <!-- GET FREE CONSULTATION -->


    <!-- FOOTER -->
    <footer id="footer" class="background-dark text-grey">
        <div class="footer-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget clearfix widget-categories">
                            <h4 class="widget-title">Our Services</h4>
                            <ul class="list list-arrow-icons">
                                <li><a title="" href="#">Development </a></li>
                                <li><a title="" href="#">Branding </a></li>
                                <li><a title="" href="#">Marketing </a></li>
                                <li><a title="" href="#">Branding </a></li>
                                <li><a title="" href="#">Strategy solutions</a></li>
                                <li><a title="" href="#">Copywriting </a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="widget clearfix widget-categories">
                            <h4 class="widget-title">Blog categories</h4>
                            <ul class="list list-arrow-icons">
                                <li><a href="#">Themeforest</a></li>
                                <li><a href="#">Web Design</a></li>
                                <li><a href="#">Wordpress</a></li>
                                <li><a href="#">HTML &amp; CSS</a></li>
                                <li><a href="#">Illustration</a></li>
                                <li><a href="#">Template</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="widget clearfix widget-categories">
                            <h4 class="widget-title">Links</h4>
                            <ul class="list list-arrow-icons">
                                <li><a href="#">Help Center</a>
                                </li>
                                <li><a href="#">Forum</a>
                                </li>
                                <li><a href="#">FAQ</a>
                                </li>
                                <li><a href="#">About us</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <img src="homepages/real-estate/images/logo-dark.png" alt="">

                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's</p>
                        <div class="seperator seperator-dark seperator-simple"></div>
                        <ul class="list">
                            <li><a href="#" title=""> Terms and Conditions </a></li>
                            <li><a href="#" title=""> Privacy Policy </a></li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
        <div class="copyright-content">
            <div class="container">
                <div class="row">
                    <div class="copyright-text text-center"> © 2016 POLO - Best HTML5 Template Ever. All Rights
                        Reserved. <a href="http://www.inspiro-media.com" target="_blank">INSPIRO</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END: FOOTER -->

</div>
<!-- END: WRAPPER -->

<!-- GO TOP BUTTON -->
<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>

<!-- Theme Base, Components and Settings -->
<script src="js/theme-functions.js"></script>

<!-- Custom js file -->
<script src="js/custom.js"></script>


</body>

</html>
