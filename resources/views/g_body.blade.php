<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>


    <link rel="shortcut icon" href="images/favicon.png">
    <title>Mats</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/fontawesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="vendor/animateit/animate.min.css" rel="stylesheet">

    <!-- Vendor css -->
    <link href="vendor/owlcarousel/owl.carousel.css" rel="stylesheet">
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Template base -->
    <link href="css/theme-base.css" rel="stylesheet">

    <!-- Template elements -->
    <link href="css/theme-elements.css" rel="stylesheet">

    <!-- Responsive classes -->
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->


    <!-- Template color -->
    <link href="css/color-variations/green.css" rel="stylesheet" type="text/css" media="screen">

    <!-- LOAD GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800"
          rel="stylesheet" type="text/css"/>


    <!-- CSS real-estate STYLE -->
    <link rel="stylesheet" type="text/css" href="homepages/real-estate/css/real-estate-style.css" media="screen"/>

    <!-- CSS CUSTOM STYLE -->
    <link rel="stylesheet" type="text/css" href="css/custom.css" media="screen"/>

    <!--VENDOR SCRIPT-->
    <script src="vendor/jquery/jquery-1.11.2.min.js"></script>
    <script src="vendor/plugins-compressed.js"></script>
    <style>
        #header-wrap {
            background: #15a74c !important;
        }

        #mainMenu > ul > li > a {
            color: white !important;
        }

        #gallery {
            padding-top: 0 !important;
        }

        .background-colored {
            background-color: #15A74C !important;
        }

        ul.dropdown-menu {
            background: #15A74C !important;
        }

        ul.dropdown-menu > li > a {
            color: white !important;
        }
    </style>
</head>

<body class="wide">


<!-- WRAPPER -->
<div class="wrapper">


    <!-- HEADER -->
    <header id="header" class="header-modern">
        <div id="header-wrap">
            <div class="">

                <!--LOGO-->
                <div id="logo">
                    <a href="index.php" class="logo" data-dark-logo="images/mats_logo.png">
                        <img src="images/mats_logo.png">
                    </a>
                </div>
                <!--END: LOGO-->

                <!--MOBILE MENU -->
                <div class="nav-main-menu-responsive">
                    <button class="lines-button x">
                        <span class="lines"></span>
                    </button>
                </div>
                <!--END: MOBILE MENU -->

                <!--NAVIGATION-->
                <div class="navbar-collapse collapse main-menu-collapse navigation-wrap">
                    <div class="container">
                        <nav class="main-menu mega-menu" id="mainMenu">
                            <ul class="main-menu nav nav-pills">
                                <li class="">
                                    <a href="{{route('welcome')}}" class="scroll-to">Home</a>
                                </li>
                                <li class=""><a href="{{route('welcome')}}#program" class="scroll-to">About The MATS</a></li>
                                <li class="dropdown"><a href="#" class="">Governing Body</a></li>
                                <li class=""><a href="{{route('welcome')}}#mission" class="scroll-to">Mission & Vision</a></li>
                                <li class=""><a href="{{route('welcome')}}#">Facilities <i class="fa fa-angle-down"></i> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Academic</a></li>
                                        <li><a href="#">Administrative</a></li>
                                        <li><a href="#">Lab</a></li>
                                        <li><a href="#">Hospital</a></li>
                                    </ul>
                                </li>

                                <li class=""><a href="{{route('welcome')}}#teacher" class="scroll-to">Teachers List</a></li>
                                <li class="dropdown"><a href="#" class="scroll-to">Students List</a></li>
                                <li class=""><a href="{{route('welcome')}}#section6" class="scroll-to">Activates</a></li>
                                <li class=""><a href="{{route('welcome')}}#gallery" class="scroll-to">Gallery</a></li>
                                <li class=""><a href="{{route('welcome')}}#section6" class="scroll-to">Contact US</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--END: NAVIGATION-->


            </div>
        </div>
    </header>
    <!-- END: HEADER -->

    <!-- SLIDER -->
    <section id="slider" class="no-padding">

        <div id="slider-carousel" class="boxed-slider">

            <div style="background-image:url('images/slider/homee.jpg');" class="owl-bg-img">
                <div class="container-fullscreen">
                    <div class="text-middle">
                        <div class="container">
                            <div class="text-light slider-content m-t-40">
                                <h2 class="text-uppercase text-medium">Discover Your <br/>Perfect Home</h2>
                                <p class="lead">Lorem ipsum dolor sit amet, consecte adipiscing elit.
                                    <br/> Suspendisse condimentum porttitor cursumus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="background-image:url('images/slider/home4.jpg');" class="owl-bg-img">

                <div class="container-fullscreen">
                    <div class="text-middle">
                        <div class="container">
                            <div class="text-light slider-content  m-t-40">
                                <h2 class="text-uppercase text-medium no-margin">Beach Home</h2>
                                <h2 class="text-uppercase text-large no-margin">199.000$</h2>
                                <p class="lead">Lorem ipsum dolor sit amet, consecte adipiscing elit.
                                    <br/> Suspendisse condimentum porttitor cursumus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="background-image:url('images/slider/home1.jpg');" class="owl-bg-img">

                <div class="container-fullscreen">
                    <div class="text-middle">
                        <div class="container">
                            <div class="text-light slider-content  m-t-40">
                                <h2 class="text-uppercase text-medium no-margin">Beach Home</h2>
                                <h2 class="text-uppercase text-large no-margin">199.000$</h2>
                                <p class="lead">Lorem ipsum dolor sit amet, consecte adipiscing elit.
                                    <br/> Suspendisse condimentum porttitor cursumus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END: SLIDER -->


    <!-- SECTION -->
    <section>
        <div class="container portfolio">

            <!--Portfolio Filter-->
            <!-- <div class="filter-active-title">Show All</div>
             <ul class="portfolio-filter" id="portfolio-filter" data-isotope-nav="isotope">
                 <li class="ptf-active" data-filter="*">Show All</li>
                 <li data-filter=".artwork">Artwork</li>
                 <li data-filter=".banner">Banner</li>
                 <li data-filter=".beauty">Beauty</li>
                 <li data-filter=".marketing">Marketing</li>
                 <li data-filter=".design">Design</li>
             </ul>-->
            <!-- END: Portfolio Filter -->

            <!-- Portfolio Items -->
            <div id="isotope" class="isotope portfolio-items" data-isotope-col="1" data-isotope-item=".portfolio-item">
                <div class="portfolio-item design">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/1.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Fast Skateboard</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>

                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design beauty">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/2.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Working hard</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design beauty">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/3.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">The feather man</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design artwork">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/4.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">The long line</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item banner beauty">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/5.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Backwards</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design artwork banner">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/6.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Disappointed horse</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design artwork">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/7.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Wire's</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design marketing">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/8.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Forcing</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design marketing banner">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/9.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">No words</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design marketing banner">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/10.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>

                    <div class="portfolio-description">
                        <h4 class="title">Baloon</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design artwork banner">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/11.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Hidden girl</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design artwork">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/12.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Yellow box</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>



                <div class="portfolio-item design marketing banner">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/13.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Traveling</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design artwork banner">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/14.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Free bird</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

                <div class="portfolio-item design artwork">
                    <div class="portfolio-image effect social-links">
                        <img src="images/portfolio/15.jpg" alt="">
                        <div class="image-box-content">
                            <p>
                                <a href="images/portfolio/1.jpg" data-lightbox-type="image" title="Your image title here!"><i class="fa fa-expand"></i></a>
                                <a href="portfolio-page-basic.html"><i class="fa fa-link"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="portfolio-description">
                        <h4 class="title">Kameleon</h4>
                        <p><i class="fa fa-tag"></i>Design / Artwork</p>
                    </div>
                    <div class="portfolio-date">
                        <p class="small"><i class="fa fa-calendar-o"></i>April 26, 2015</p>
                    </div>
                    <div class="portfolio-details">
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam, non ornare orci ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur fringilla luctus. Fusce id mi diam.</p>
                        <br />
                        <a href="#" class="button color rounded button-3d effect icon-top"><span><i class="fa fa-external-link"></i>More details</span></a>
                    </div>
                </div>

            </div>
            <!-- END: Portfolio Items -->


        </div>

        <hr class="space">

    </section>
    <!-- FOOTER -->
    <footer id="footer" class="background-dark text-grey">
        <div class="footer-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget clearfix widget-categories">
                            <h4 class="widget-title">Our Services</h4>
                            <ul class="list list-arrow-icons">
                                <li><a title="" href="#">Development </a></li>
                                <li><a title="" href="#">Branding </a></li>
                                <li><a title="" href="#">Marketing </a></li>
                                <li><a title="" href="#">Branding </a></li>
                                <li><a title="" href="#">Strategy solutions</a></li>
                                <li><a title="" href="#">Copywriting </a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="widget clearfix widget-categories">
                            <h4 class="widget-title">Blog categories</h4>
                            <ul class="list list-arrow-icons">
                                <li><a href="#">Themeforest</a></li>
                                <li><a href="#">Web Design</a></li>
                                <li><a href="#">Wordpress</a></li>
                                <li><a href="#">HTML &amp; CSS</a></li>
                                <li><a href="#">Illustration</a></li>
                                <li><a href="#">Template</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="widget clearfix widget-categories">
                            <h4 class="widget-title">Links</h4>
                            <ul class="list list-arrow-icons">
                                <li><a href="#">Help Center</a>
                                </li>
                                <li><a href="#">Forum</a>
                                </li>
                                <li><a href="#">FAQ</a>
                                </li>
                                <li><a href="#">About us</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <img src="homepages/real-estate/images/logo-dark.png" alt="">

                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's</p>
                        <div class="seperator seperator-dark seperator-simple"></div>
                        <ul class="list">
                            <li><a href="#" title=""> Terms and Conditions </a></li>
                            <li><a href="#" title=""> Privacy Policy </a></li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
        <div class="copyright-content">
            <div class="container">
                <div class="row">
                    <div class="copyright-text text-center"> © 2016 POLO - Best HTML5 Template Ever. All Rights
                        Reserved. <a href="http://www.inspiro-media.com" target="_blank">INSPIRO</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END: FOOTER -->

</div>
<!-- END: WRAPPER -->

<!-- GO TOP BUTTON -->
<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>

<!-- Theme Base, Components and Settings -->
<script src="js/theme-functions.js"></script>

<!-- Custom js file -->
<script src="js/custom.js"></script>


</body>

</html>
